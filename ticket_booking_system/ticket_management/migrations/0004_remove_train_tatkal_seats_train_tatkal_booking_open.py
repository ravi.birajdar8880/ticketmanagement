# Generated by Django 4.2.7 on 2023-11-12 05:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ticket_management', '0003_ticket_seat_no'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='train',
            name='tatkal_seats',
        ),
        migrations.AddField(
            model_name='train',
            name='tatkal_booking_open',
            field=models.BooleanField(default=False),
        ),
    ]
