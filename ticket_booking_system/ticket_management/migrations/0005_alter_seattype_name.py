# Generated by Django 4.2.7 on 2023-11-12 06:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ticket_management', '0004_remove_train_tatkal_seats_train_tatkal_booking_open'),
    ]

    operations = [
        migrations.AlterField(
            model_name='seattype',
            name='name',
            field=models.CharField(choices=[('1A', '1A'), ('2A', '2A'), ('3A', '3A'), ('SL', 'SL  '), ('2S', '2S'), ('UR', 'UR'), ('TK', 'TK')], max_length=20),
        ),
    ]
