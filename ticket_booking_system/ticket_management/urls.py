
from django.urls import path
from rest_framework.routers import DefaultRouter
from .views import TicketViewSet

# from django.urls import reverse

router = DefaultRouter()

urlpatterns = router.urls

urlpatterns += [
    path('book-ticket/', TicketViewSet.as_view({'post': 'book_ticket'}), name='book-ticket'),
    path('cancel-ticket/', TicketViewSet.as_view({'post': 'cancel_ticket'}), name='cancel-ticket'),
    path('seat-info/', TicketViewSet.as_view({'post': 'get_seat_information'}), name='seat-info'),
    path('tatkal-ticket-booking/', TicketViewSet.as_view({'post': 'tatkal_ticket_booking'}), name='tatkal-ticket-booking'),
]
