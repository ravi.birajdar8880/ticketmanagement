from django.contrib import admin
from .models import *
# Register your models here.

# admin.site.register(Route)
# admin.site.register(Train)
# admin.site.register(SeatType)


from django.contrib import admin
from .models import User
from django.apps import apps


exclude_models = ['AbstractUser', 'AbstractBaseUser']
app_models = apps.get_app_config('ticket_management').get_models()

for model in app_models:
    if model.__name__ not in exclude_models:
        if model.__name__ in ['SeatType','Route','RouteSeatType', 'Train']:
            continue
        else:
            admin.site.register(model)

from django.contrib import admin
from .models import SeatType, Route, RouteSeatType, Train

@admin.register(SeatType)
class SeatTypeAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')

@admin.register(Route)
class RouteAdmin(admin.ModelAdmin):
    list_display = ('id', 'from_station', 'to_station')

@admin.register(RouteSeatType)
class RouteSeatTypeAdmin(admin.ModelAdmin):
    list_display = ('id', 'route', 'seat_type', 'base_price_adjustment')

@admin.register(Train)
class TrainAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'train_no', 'departure_time', 'route', 'tatkal_booking_open')
    filter_horizontal = ('available_seat_types',)  # for ManyToManyField
