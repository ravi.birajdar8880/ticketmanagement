import random
from django.db import models
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.contrib.auth.models import User
from users.models import BaseModel

class SeatType(models.Model):
    SEAT_TYPE_CHOICES = [
        ('1A', '1A'),
        ('2A', '2A'),
        ('3A', '3A'),
        ('SL', 'SL  '),
        ('2S', '2S'),
        ('UR', 'UR'),
    ]
    name = models.CharField(max_length=20, choices=SEAT_TYPE_CHOICES)

    def __str__(self):
        return self.name


class Route(models.Model):
    from_station = models.CharField(max_length=100)
    to_station = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.from_station + "-->" + self.to_station


class RouteSeatType(models.Model):
    route = models.ForeignKey(Route, on_delete=models.CASCADE)
    seat_type = models.ForeignKey(SeatType, on_delete=models.CASCADE)
    base_price_adjustment = models.DecimalField(max_digits=10, decimal_places=2, default=0.0)

    def __str__(self):
        return self.seat_type.name


class Train(models.Model):
    name = models.CharField(max_length=100)
    train_no = models.IntegerField()
    departure_time = models.DateTimeField()
    route = models.ForeignKey(Route, on_delete=models.CASCADE)
    available_seat_types = models.ManyToManyField(SeatType)
    tatkal_booking_open = models.BooleanField(default=False)


    def __str__(self):
        return self.name +'('+ str(self.train_no) +')'

    def get_seat_information(self, route_id, seat_type_id):
        try:
            route = Route.objects.get(id=route_id)
            seat_type = SeatType.objects.get(id=seat_type_id)
            train_seat_type = self.trainseattype_set.get(seat_type=seat_type)
        except ObjectDoesNotExist:
            return None

        booked_tickets = Ticket.objects.filter(train=self, route=route, seat_type=seat_type, status='Booked').count()
        waiting_tickets = Ticket.objects.filter(train=self, route=route, seat_type=seat_type, status='Waiting').count()
        available_seats = train_seat_type.available_seats

        seat_information = {
            "Train Name": self.name,
            "From Station": route.from_station,
            "To Station": route.to_station,
            "seat_type" : seat_type.name,
            'booked_tickets': booked_tickets,
            'waiting_tickets': waiting_tickets,
            'available_seats': available_seats,
        }

        return seat_information
    

    def is_tatkal_window_open(self):
        return self.tatkal_booking_open

class TrainSeatType(models.Model):
    train = models.ForeignKey(Train, on_delete=models.CASCADE)
    seat_type = models.ForeignKey(SeatType, on_delete=models.CASCADE)
    total_seats = models.IntegerField(default=0)
    available_seats = models.IntegerField(default=0)

    def __str__(self):
        return f"{self.train} - {self.seat_type}"

    def increment_available_seats(self):
        self.available_seats += 1
        self.save()

class WaitingList(BaseModel,models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    train = models.ForeignKey(Train, on_delete=models.CASCADE)
    ticket_type = models.CharField(max_length=20, blank=True, null=True)
    waiting_number = models.PositiveIntegerField(null=True, blank=True)
    booking_type = models.CharField(max_length=10, choices=[('normal', 'Normal'), ('tatkal', 'Tatkal')], default='Normal')

    def save(self, *args, **kwargs):
        current_waiting_count = WaitingList.objects.filter(train=self.train, ticket_type=self.ticket_type).count()
        if current_waiting_count == 1:
            self.waiting_number = 1
        else:
            self.waiting_number = current_waiting_count + 1
        super().save(*args, **kwargs)

class Ticket(models.Model):
    STATUS_CHOICES = [
        ('Booked', 'Booked'),
        ('Waiting', 'Waiting'),
        ('Cancelled', 'Cancelled'),
    ]
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    train = models.ForeignKey(Train, on_delete=models.CASCADE)
    route = models.ForeignKey(Route, on_delete=models.CASCADE)
    seat_type = models.ForeignKey(SeatType, on_delete=models.CASCADE)
    status = models.CharField(max_length=20, choices=STATUS_CHOICES, default='Waiting')
    pnr_number = models.CharField(max_length=10, unique=True, blank=True)
    seat_no = models.IntegerField(default=0)
    price = models.DecimalField(max_digits=10, decimal_places=2, default=0.0)
    booking_type = models.CharField(max_length=10, choices=[('normal', 'Normal'), ('tatkal', 'Tatkal')], default='Normal')

    def __str__(self):
        return self.pnr_number

    def check_and_book(self):
        if self.booking_type == "tatkal":
            try:
                tatkal = TatkalBooking.objects.get(train=self.train, route=self.route, seat_type=self.seat_type)
                if tatkal.available_seats > 0:
                    if tatkal.available_seats == tatkal.total_seats:
                        self.seat_no = 1
                    else:
                        booked_tickets = Ticket.objects.filter(
                            train=self.train,
                            route=self.route,
                            seat_type=self.seat_type,
                            status='Booked',
                            booking_type="tatkal"
                        ).count()
                        self.seat_no = 1 + booked_tickets
                    tatkal.available_seats -= 1
                    tatkal.save()
                    self.status = 'Booked'
                    return True

                else:
                    waiting_tickets = Ticket.objects.filter(
                        train=self.train,
                        route=self.route,
                        seat_type=self.seat_type,
                        status='Waiting',
                        booking_type="tatkal"
                    ).count()
                    self.seat_no = 1 + waiting_tickets
                    return False

            except (TatkalBooking.DoesNotExist):
                return None

        else:
            try:
                train_seat_type = self.train.trainseattype_set.get(seat_type=self.seat_type)
            except TrainSeatType.DoesNotExist:
                raise ValidationError("Invalid seat type for the train.")

            total_seats = train_seat_type.total_seats
            available_seats = train_seat_type.available_seats

            if train_seat_type.available_seats > 0:

                if total_seats == available_seats:
                    self.seat_no = 1
                else:
                    booked_tickets = Ticket.objects.filter(
                        train=self.train,
                        route=self.route,
                        seat_type=self.seat_type,
                        status='Booked',
                        booking_type="normal"
                    ).count()
                    self.seat_no = 1 + booked_tickets

                train_seat_type.available_seats -= 1
                train_seat_type.save()
                self.status = 'Booked'
                return True
            else:
                self.status = 'Waiting'
                waiting_tickets = Ticket.objects.filter(
                    train=self.train,
                    route=self.route,
                    seat_type=self.seat_type,
                    booking_type="normal",
                    status='Waiting'
                ).count()
                self.seat_no = 1 + waiting_tickets
                return False

    def calculate_price(self):
        try:
            route_seat_type = RouteSeatType.objects.get(route=self.route, seat_type=self.seat_type)
        except RouteSeatType.DoesNotExist:
            return None
        base_price_adjustment = route_seat_type.base_price_adjustment
        final_price = base_price_adjustment
        return final_price

    def calculate_takal_price(self):
        try:
            tatkal_seat = TatkalBooking.objects.get(train=self.train, route=self.route, seat_type=self.seat_type)
        except TatkalBooking.DoesNotExist:
            return None
        price = tatkal_seat.price
        return price

    def _generate_pnr_number(self):
        return ''.join(str(random.randint(1, 10)) for _ in range(8))

    def save(self, *args, **kwargs):
        if not self.pk:
            self.pnr_number = self._generate_pnr_number()
            if self.booking_type == "tatkal":
                self.price = self.calculate_takal_price()
                tatkal_obj = TatkalBooking.objects.get(train = self.train, route = self.route, seat_type = self.seat_type)
                if tatkal_obj.available_seats > 0:
                    tatkal_obj.available_seats -= 1
            else:
                self.price = self.calculate_price()
            if self.check_and_book() is False:
                waiting_obj = WaitingList.objects.create(user=self.user, train=self.train,
                                                         ticket_type=self.seat_type.name, booking_type=self.booking_type)
                waiting_obj.save()
        super().save(*args, **kwargs)

class CancelledTicket(BaseModel, models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    train = models.ForeignKey(Train, on_delete=models.CASCADE)
    seat_type = models.ForeignKey(SeatType, on_delete=models.CASCADE)
    route = models.ForeignKey(Route, on_delete=models.CASCADE)
    pnr_number = models.CharField(max_length=10, unique=True)

    def __str__(self):
        return f"{self.user.first_name} - {self.train.name} ({self.pnr_number})"

class TatkalBooking(models.Model):
    train = models.ForeignKey('Train', on_delete=models.CASCADE)
    route = models.ForeignKey('Route', on_delete=models.CASCADE)
    seat_type = models.ForeignKey('SeatType', on_delete=models.CASCADE)
    total_seats = models.IntegerField(default=0)
    available_seats = models.IntegerField(default=10)
    price = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)

    def increment_available_seats(self):
        self.available_seats += 1
        self.save()