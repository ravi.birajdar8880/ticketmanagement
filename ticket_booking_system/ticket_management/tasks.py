from celery import shared_task
from django.core.mail import send_mail
from .models import Train, WaitingList
from django.conf import settings

@shared_task
def open_tatkal_window(id):
    """
    Celery task to open the Tatkal booking window for a given Train instance by setting tatkal_booking_open to True.
    """
    train = Train.objects.get(id=id)
    train.tatkal_booking_open = True
    train.save()

@shared_task
def close_tatkal_window(id):
    """
    Celery task to close the Tatkal booking window for a given Train instance by setting tatkal_booking_open to False.
    """
    train = Train.objects.get(id=id)
    train.tatkal_booking_open = False
    train.save()

@shared_task
def notify_waiting_list_users(id):
    """
    Celery task to notify users on the waiting list about the Tatkal booking window for a given Train instance.
    """
    train = Train.objects.get(id=id)
    waiting_list_users = WaitingList.objects.filter(train=train)
    if waiting_list_users:
        for user in waiting_list_users:
            subject = 'Tatkal Booking Window Opening Soon'
            message = f'The Tatkal booking window for train {train.name} will open in 5 minutes.'
            from_email = settings.EMAIL_HOST_USER
            recipient_list = [user.email]
            send_mail(subject, message, from_email, recipient_list)
    else:
        print(f"There are no waiting passengers for {train.name} train")
