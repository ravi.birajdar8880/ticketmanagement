from django.apps import AppConfig


class TicketManagementConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ticket_management'

    def ready(self):
        try:
            import ticket_management.signal
        except Exception as e:
            pass

