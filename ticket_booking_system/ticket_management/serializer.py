from rest_framework import serializers
from .models import *


class SeatTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = SeatType
        fields = ['name']


class TrainSerializer(serializers.ModelSerializer):
    class Meta:
        model = Train
        fields = ['train_no', 'name', 'departure_time']


class RouteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Route
        fields = ['from_station', 'to_station']


class TicketSerializer2(serializers.ModelSerializer):
    """Serializer for Ticket with 'pnr_number' field."""

    class Meta:
        model = Ticket
        fields = ['pnr_number']


class TicketSerializer(serializers.ModelSerializer):
    """Serializer for Ticket including related Train, Route, and SeatType."""

    train = TrainSerializer()
    route = RouteSerializer()
    seat_type = SeatTypeSerializer()

    class Meta:
        model = Ticket
        fields = ['train', 'route', 'seat_type', 'pnr_number', 'status', 'seat_no', 'booking_type']

    def to_representation(self, instance):
        """Custom representation of Ticket's status."""
        representation = super().to_representation(instance)

        if instance.status == 'Booked':
            representation['status'] = f'CNF/{str(instance.seat_no)}'
        elif instance.status == 'Waiting':
            representation['status'] = f'WL/{str(instance.seat_no)}'
            representation.pop('seat_no', None)

        return representation

    def to_internal_value(self, data):
        """Convert related objects to their respective instances."""
        data['train'] = Train.objects.get(pk=data['train'])
        data['route'] = Route.objects.get(pk=data['route'])
        data['seat_type'] = SeatType.objects.get(pk=data['seat_type'])
        return data

    def create(self, validated_data):
        """Create a Ticket."""
        login_user = self.context.get('request', None)
        if validated_data['booking_type'] == 'tatkal':
            is_open = validated_data['train'].is_tatkal_window_open()
            if is_open:
                ticket = Ticket.objects.create(user=login_user.user, **validated_data)
                ticket.save()
            else:
                raise serializers.ValidationError("tatkal window is not open")
        else:
            ticket = Ticket.objects.create(user=login_user.user, **validated_data)
            ticket.save()
        return ticket


class TatkalTicketSerializer(serializers.ModelSerializer):
    """Serializer for Tatkal Ticket including related Train, Route, and SeatType."""

    train = TrainSerializer()
    route = RouteSerializer()
    seat_type = SeatTypeSerializer()

    class Meta:
        model = Ticket
        fields = ['train', 'route', 'seat_type', 'pnr_number', 'status', 'seat_no']

    def to_internal_value(self, data):
        """Convert related objects to their respective instances."""
        data['train'] = Train.objects.get(pk=data['train'])
        data['route'] = Route.objects.get(pk=data['route'])
        data['seat_type'] = SeatType.objects.get(pk=data['seat_type'])
        return data

    def create(self, validated_data):
        """Create a Tatkal Ticket."""
        login_user = self.context.get('request', None)
        ticket = Ticket.objects.create(user=login_user.user, **validated_data)
        ticket.save()
        return ticket


class CancelledTicketSerializer(serializers.ModelSerializer):
    """Serializer for Cancelled Ticket."""

    class Meta:
        model = CancelledTicket
        fields = '__all__'

    def create(self, validated_data):
        """
        Create a Cancelled Ticket.

        Actions:
        - Increments available seats for regular or Tatkal bookings.
        - Handles waiting list entry deletion.
        """
        cancelled_ticket = super().create(validated_data)
        try:
            ticket = Ticket.objects.get(pnr_number=validated_data['pnr_number'])
            if ticket.booking_type != 'tatkal':
                train_seat_type = TrainSeatType.objects.get(train=cancelled_ticket.train,
                                                            seat_type=cancelled_ticket.seat_type)
                train_seat_type.increment_available_seats()
            else:
                tatkal_seat_type = TatkalBooking.objects.get(train=cancelled_ticket.train,
                                                             route=validated_data['route'],
                                                             seat_type=cancelled_ticket.seat_type)
                tatkal_seat_type.increment_available_seats()
        except TrainSeatType.DoesNotExist:
            pass
        try:
            waiting_list_entry = WaitingList.objects.filter(train=cancelled_ticket.train,
                                                            ticket_type=cancelled_ticket.seat_type,
                                                            booking_type=ticket.booking_type).first()
            if waiting_list_entry:
                current = ticket
                current.waiting_list_entry = waiting_list_entry
                waiting_list_entry.delete()
                current.save()
        except WaitingList.DoesNotExist:
            Ticket.objects.get(pnr_number=validated_data['pnr_number']).delete()

        return cancelled_ticket
