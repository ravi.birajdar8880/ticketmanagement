from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status
from .models import Ticket, Train
from .serializer import TicketSerializer, CancelledTicketSerializer
from rest_framework.exceptions import ValidationError
from rest_framework_simplejwt.authentication import JWTAuthentication

class TicketViewSet(viewsets.ModelViewSet):
    serializer_class = TicketSerializer
    authentication_classes = [JWTAuthentication]
    def book_ticket(self, request):
        """
        Book a train ticket.

        Parameters:
        - train: int
        - route: int
        - seat_type: int
        - booking_type: str

        Returns:
        - JSON response
        """
        try:
            serializer = self.get_serializer(data=request.data, context={"request": request})
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        except ValidationError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({"error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def cancel_ticket(self, request):
        """
        Cancel a train ticket.

        Parameters:
        - pnr_number: str

        Returns:
        - JSON response
        """
        try:
            pnr_number = request.data.get('pnr_number')
            ticket = Ticket.objects.get(pnr_number=pnr_number)
        except Ticket.DoesNotExist:
            return Response({'error': 'Ticket not found or not booked'}, status=status.HTTP_404_NOT_FOUND)

        cancelled_ticket_serializer = CancelledTicketSerializer(data={
            'user': ticket.user.id,
            'train': ticket.train.id,
            'seat_type': ticket.seat_type.id,
            'route': ticket.route.id,
            'pnr_number': ticket.pnr_number,
        })

        if cancelled_ticket_serializer.is_valid():
            cancelled_ticket_serializer.save()
            return Response({'success': 'Ticket cancelled successfully'}, status=status.HTTP_200_OK)
        return Response({'error': 'This ticket is already got cancelled'}, status=status.HTTP_400_BAD_REQUEST)

    def get_seat_information(self, request):
        """
        Get information about seat availability.

        Parameters:
        - train: int
        - route: int
        - seat_type: int

        Returns:
        - JSON response
        """
        try:
            train = Train.objects.get(id=request.data.get('train'))
        except Train.DoesNotExist:
            return Response({'error': 'Train not found'}, status=404)

        seat_information = train.get_seat_information(request.data.get('route'), request.data.get('seat_type'))

        if seat_information is None:
            return Response({'error': 'Invalid route or seat type for the train'}, status=400)

        return Response(seat_information)

    def tatkal_ticket_booking(self, request):
        """
        Book a Tatkal train ticket.

        Parameters:
        - train: int
        - route: int
        - seat_type: int
        - booking_type: str

        Returns:
        - JSON response
        """
        try:
            serializer = self.get_serializer(data=request.data, context={"request": request})
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        except ValidationError as e:
            error_message = str(e.detail[0]) if isinstance(e.detail, list) else str(e.detail)
            return Response({'error': error_message},status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({"error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
