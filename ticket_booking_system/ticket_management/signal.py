from django.dispatch import receiver
from django.db.models.signals import post_save
from django.utils import timezone
from django.db.models import Q
from django_celery_beat.models import PeriodicTask, CrontabSchedule
from .models import Train

def calculate_notification_schedule(notification_time):
    """
    Calculate the schedule for notifications based on the provided notification time.
    """
    return {
        'minute': f"{notification_time.minute}",
        'hour': f"{notification_time.hour}",
        'day_of_month': '*',
        'month_of_year': '*',
    }

def create_crontab_schedule(schedule_params):
    """
    Create a crontab schedule based on the provided schedule parameters.
    """
    return CrontabSchedule.objects.create(
        minute=schedule_params.get('minute', '*'),
        hour=schedule_params.get('hour', '*'),
        day_of_month=schedule_params.get('day_of_month', '*'),
        month_of_year=schedule_params.get('month_of_year', '*')
    )

def periodic_tasks_exist(train):
    """
    Check if periodic tasks exist for a given train instance.
    """
    return PeriodicTask.objects.filter(
        Q(name=f'Notify_Train_{train.id}') |
        Q(name=f'Closed_Train_{train.id}') |
        Q(name=f'Open_Train_{train.id}')
    ).exists()

def create_periodic_task_for_train(train):
    """
    Create periodic tasks associated with the train instance if they don't already exist.
    """
    if not periodic_tasks_exist(train):
        open_tatkal_window_schedule = calculate_notification_schedule(train.departure_time - timezone.timedelta(minutes=120))
        open_tatkal_window_crontab = create_crontab_schedule(open_tatkal_window_schedule)

        open_tatkal_window_task = PeriodicTask.objects.get_or_create(
            name=f'Open_Train_{train.id}',
            task='ticket_management.tasks.open_tatkal_window',
            crontab=open_tatkal_window_crontab,
            args=[train.id],
        )

        close_tatkal_window_schedule = calculate_notification_schedule(train.departure_time - timezone.timedelta(minutes=110))
        close_tatkal_window_crontab = create_crontab_schedule(close_tatkal_window_schedule)

        close_tatkal_window_task = PeriodicTask.objects.create(
            name=f'Closed_Train_{train.id}',
            task='ticket_management.tasks.close_tatkal_window',
            crontab=close_tatkal_window_crontab,
            args=[train.id],
        )

        notify_waiting_list_schedule = calculate_notification_schedule(train.departure_time - timezone.timedelta(minutes=125))
        notify_waiting_list_crontab = create_crontab_schedule(notify_waiting_list_schedule)

        notify_waiting_list_task = PeriodicTask.objects.create(
            name=f'Notify_Train_{train.id}',
            task='ticket_management.tasks.notify_waiting_list_users',
            crontab=notify_waiting_list_crontab,
            args=[train.id],
        )

@receiver(post_save, sender=Train)
def train_periodic_task(sender, instance, created, **kwargs):
    """
    Listen to the post_save signal for Train model instances.
    Create periodic tasks associated with the train instance.
    """
    create_periodic_task_for_train(instance)
