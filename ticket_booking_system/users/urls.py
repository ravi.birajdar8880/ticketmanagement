from django.urls import path
from rest_framework.routers import DefaultRouter
from .views import UserViewSet
# from django.urls import reverse

router = DefaultRouter()


urlpatterns = router.urls

urlpatterns += [
    path('register/', UserViewSet.as_view({'post': 'register'}), name='user-register'),
    path('login/', UserViewSet.as_view({'post': 'login'}), name='user-login'),

    ]