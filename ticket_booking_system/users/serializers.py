from rest_framework import serializers
from .models import *
import re


class UserRegistrationSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)

    class Meta:
        model = User
        fields = ['username', 'password','first_name', 'last_name', 'email']

    # def validate_username(self, value):
    #
    #     # Check if username contains only letters (no digits or special characters)
    #     if not re.match("^[a-zA-Z]+$", value):
    #         raise serializers.ValidationError("Username should only contain letters.")
    #
    #     return value

    # def validate_password(self, value):
    #
    #     if len(value) < 8:
    #         raise serializers.ValidationError("Password should be at least 8 characters long.")
    #
    #     if not re.search("[!@#$%^&*(),.?\":{}|<>]", value):
    #         raise serializers.ValidationError("Password should contain at least one special character.")
    #
    #     return value

    def create(self, validated_data):
        user = User(username=validated_data['username'],
                first_name =validated_data['first_name'],
                last_name=validated_data['last_name'],
                email=validated_data['email']
        )
        user.set_password(validated_data['password'])
        user.save()
        return user
