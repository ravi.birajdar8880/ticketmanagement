from django.db import models
from django.contrib.auth.models import BaseUserManager,AbstractBaseUser
from .managers import *
class BaseModel(models.Model):
    """
    Data required in every table is created in this model and inherited by below models.
    """

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_deleted = models.BooleanField(default=False)

    class Meta:
        abstract = True



class User(AbstractBaseUser, BaseModel):
    """
    User model stores all user related information.
    """
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    email = models.EmailField(max_length=255,unique=True)
    username = models.CharField(max_length=255, unique=True)
    status = models.BooleanField(default=True)


    objects = UserManager()

    USERNAME_FIELD = "username"
    REQUIRED_FIELDS = []

    class Meta:
        db_table = "user"
        verbose_name_plural = "Users"


