# Ticket Management

## Description

This is train ticket booking Management application project with sample API'S


## Table of Contents

1. [Installation](#installation)
2. [Usage](#usage)
3. [Endpoints](#endpoints)
    - [Register](#1-register)
    - [Login](#2-login)
    - [Book Ticket](#3-book-ticket)
    - [Cancel Ticket](#4-cancel-ticket)
    - [Seat Info](#5-seat-info)
    - [Tatkal Ticket Booking](#6-tatkal-ticket-booking)



## Installation

Describe how to install and set up the project. Include any dependencies and their installation instructions.

```bash
pip install -r requirements.txt
```

## Run Server

```bash
python manage.py runserver

```   

## Endpoints

| Endpoint                | Description                                   | Method | Request Body                                       | Response                                                                                                                                               |
|-------------------------|-----------------------------------------------|--------|----------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------|
| `/register/`            | Register a new user.                          | `POST` | `{ "first_name":"ravi", "last_name":"birajdar","username": "example_user", "password": "secure_password", "email": "user@example.com" }` | `{ "message": "User registered successfully" }`                                                                                                        |
| `/login/`               | Log in an existing user.                      | `POST` | `{ "username": "example_user", "password": "secure_password" }` | `{ "token": "your_access_token" }`                                                                                                                     |
| `/book-ticket/`         | Book a ticket for a specific journey.          | `POST` | `{ "train": id, "route": id,"seat_type":id }`      | `{ "message": "Ticket booked successfully" } with informative response`                                                                                |
| `/cancel-ticket/`       | Cancel a booked ticket.                        | `POST` | `{ "pnr_number": 456 }`                           | `{ "message": "Ticket canceled successfully" }`                                                                                                        |
| `/seat-info/`           | Get information about available seats.        | `POST`  | `{ "train": id, "route": id,"seat_type":id }`                                                | `{train name:"xyz", "from station":"xyz", "to station": "abc", "booked tickets":54, "waiting tickets":87, "avaialable seats":14,"seat types":General}` |
| `/tatkal-ticket-booking/`| Book a ticket under Tatkal quota.              | `POST` | `{"train":5, "route":1, "seat_type":1, "booking_type":"tatkal"}`      | `{ "message": "Tatkal ticket booked successfully"  } with informative response`                                                                        |


## Scheduler 

```bash

celery -A ticket_booking_system  worker --loglevel=info
celery -A ticket_booking_system  beat --loglevel=info
```



